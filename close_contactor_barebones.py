import canopen
import can
import can.interfaces.ixxat
import cantools
from time import sleep
import tkinter as tk

__version__ = '0.1.0'

class bms_can():
    def __init__(self):
        # load database file
        self.database = cantools.db.load_file('Zero-complete-Powertrain-DBC-for-Polaris_v0.2_2022-4-12.dbc')
        
        # initialize pdo signals we want to transmit to the BMS.
        self.BMSA_RPDO_CONTROL = {
            "BMSA_CNTRL_DEST_NODE_ID": 0x0A,         
            "BMSA_CNTRL_UNUSED_BYTE_1": 0,         
            "BMSA_CNTRL_MBB_FIRM_ID": 7,         
            "BMSA_CNTRL_SYS_ON":  1,         
            "BMSA_CNTRL_LOCK":  1,         
            "BMSA_CNTRL_STRG_MODE_STRT": 0,         
            "BMSA_CNTRL_UNUSED_BIT_1":  0,         
            "BMSA_CNTRL_CHRGR_CON":  0,         
            "BMSA_CNTRL_CHRGR_EN":  0,         
            "BMSA_CNTRL_UNUSED_BIT_2":  0,         
            "BMSA_CNTRL_UNUSED_BIT_3":  0,         
            "BMSA_CNTRL_ALLOW_ISO_TST": 1,         
            "BMSA_CNTRL_CONCT_ISO_CIR": 0,         
            "BMSA_CNTRL_INHIBIT_FAN":  0,         
            "BMSA_CNTRL_UNUSED_BIT_5":  0,         
            "BMSA_CNTRL_ENABLE_HEATER": 0,         
            "BMSA_CNTRL_INHIBIT_HEATER": 0,         
            "BMSA_CNTRL_UNUSED_BIT_8": 0,         
            "BMSA_CNTRL_UNUSED_BIT_9": 0,         
            "BMSA_CNTRL_UNUSED_SHORT_1": 0         
        }

        self.BMSA_RPDO_CONT_PRECHG = {
            "BMSA_CONTPRE_DEST_NODE_ID": 0x0A,         
            "BMSA_CONTPRE_PRECHRG_CMD": 3,         
            "BMSA_CONTPRE_CONTACTOR_CMD": 5,         
            "BMSA_CONTPRE_UNUSED_BYTE_1":  0
        }    

        self.BMSA_RPDO_ISOLATION = {
            "BMSA_ISO_DEST_NODE_ID": 0x0A,            
            "BMSA_ISO_PULL_DOWN": 0,         
            "BMSA_ISO_PULL_UP": 0,             
            "BMSA_ISO_UNUSED_BYTE_1": 0
        }

    def init_network(self):
        # # encode initial pdo messages for transmit
        self.BMSA_RPDO_CONTROL_ENCODED = self.database.encode_message('BMSA_RPDO_CONTROL', self.BMSA_RPDO_CONTROL)
        self.BMSA_RPDO_CONT_PRECHG_ENCODED = self.database.encode_message('BMSA_RPDO_CONT_PRECHG', self.BMSA_RPDO_CONT_PRECHG)
        self.BMSA_RPDO_ISOLATION_ENCODED = self.database.encode_message('BMSA_RPDO_ISOLATION', self.BMSA_RPDO_ISOLATION)

        # initialize canopen network
        self.network = canopen.Network()

        # define tool used, channel, baud, etc.
        self.network.connect(bustype='vector', app_name='CANalyzer', channel=0, bitrate=500000)
        # self.network.connect(bustype='ixxat', channel=0, bitrate=500000)

        # start cyclic send of SYNC (0x80) heartbeat message
        self.network.sync.start(0.1)

        # perform LSS to find unconfigured devices on the bus
        # Just looking for one BMS to set as 0x0A
        node_id = 0x0A # Only support for BMSA currently.
        slave_found, slave_info = self.network.lss.fast_scan()
        if slave_found == True:
            self.network.lss.configure_node_id(node_id)
            print("Unconfigured slave found!")
            print(f'Vendor ID: {slave_info[0]}, {hex(slave_info[0])}')
            print(f'Product Code: {slave_info[1]}, {hex(slave_info[1])}')
            print(f'Revision Number: {slave_info[2]}, {hex(slave_info[2])}')
            print(f'Serial Number: {slave_info[3]}, {hex(slave_info[3])}')
            print(f'Slave with SN {slave_info[3]} assigned node id: {node_id}, {hex(node_id)}')

            sn_text.set(f'BCBM{slave_info[3]}')

            self.node = canopen.RemoteNode(node_id, 'ZeroFstBms_v0.1.eds')
            self.network.add_node(self.node)
        else:
            raise Exception("No unassigned nodes found. Try resetting the BMS")
        
        # define some params for the pdos we will be reading to allow for state feedback in procedure
        # Based on eds file tpdo 
        # rpdo[1] cob_id = 0x200 + node_id = 'BMSA_TPDO_STATUS'd
        self.node.tpdo[1].cob_id = self.node.tpdo[1].predefined_cob_id # if this isn't done, can't receive the PDOs.
        self.node.tpdo[1].enabled = True
        self.node.tpdo[1].subscribe()

        # define some params for the pdos we will be transmitting and start cyclic send
        # From the eds file, we can see the following:
        # rpdo[1] cob_id = 0x200 + node_id = 'BMSA_RPDO_CONT_PRECHG'
        # rpdo[2] cob_id = 0x300 + node_id = 'BMSA_RPDO_ISOLATION'
        # rpdo[3] cob_id = 0x400 + node_id = 'BMSA_RPDO_CONTROL'
        message_enumeration = [self.BMSA_RPDO_CONT_PRECHG_ENCODED, self.BMSA_RPDO_ISOLATION_ENCODED, self.BMSA_RPDO_CONTROL_ENCODED]
        for i, init_data in enumerate(message_enumeration, start=1):
            self.node.rpdo[i].data = list(init_data)
            self.node.rpdo[i].cob_id = self.node.rpdo[i].predefined_cob_id
            self.node.rpdo[i].start(0.1)

    def update_signals(self, message, signals):
        if message == 'BMSA_RPDO_CONTROL':
            self.BMSA_RPDO_CONTROL.update(signals)
            self.BMSA_RPDO_CONTROL_ENCODED = self.database.encode_message('BMSA_RPDO_CONTROL', self.BMSA_RPDO_CONTROL)
        elif message == 'BMSA_RPDO_CONT_PRECHG':
            self.BMSA_RPDO_CONT_PRECHG.update(signals)
            self.BMSA_RPDO_CONT_PRECHG_ENCODED = self.database.encode_message('BMSA_RPDO_CONT_PRECHG', self.BMSA_RPDO_CONT_PRECHG)
        elif message == 'BMSA_RPDO_ISOLATION':
            self.BMSA_RPDO_ISOLATION.update(signals)
            self.BMSA_RPDO_ISOLATION_ENCODED = self.database.encode_message('BMSA_RPDO_ISOLATION', self.BMSA_RPDO_ISOLATION)
        else:
            raise Exception(f'Message attempted to modify ({message}) is invalid')

        message_enumeration = [self.BMSA_RPDO_CONT_PRECHG_ENCODED, self.BMSA_RPDO_ISOLATION_ENCODED, self.BMSA_RPDO_CONTROL_ENCODED]
        for i, data in enumerate(message_enumeration, start=1): # pdos in canopen library 1 indexed
            self.node.rpdo[i].data = data
            self.node.rpdo[i].update()

    def contactor_closed(self):
        self.node.tpdo[1].wait_for_reception()
        packet = self.node.tpdo[1].data
        packet_decoded = self.database.decode_message('BMSA_TPDO_STATUS', packet)
        print(f'BMSA_STAT_CONTACTOR_CLOSED: {packet_decoded["BMSA_STAT_CONTACTOR_CLOSED"]}')
        # print(packet_decoded)
        return packet_decoded['BMSA_STAT_CONTACTOR_CLOSED']

    def close_contactor(self):
        sleep(0.1)
        
        self.update_signals('BMSA_RPDO_ISOLATION',
                            {'BMSA_ISO_PULL_DOWN': 205098})
        sleep(0.3)
        # Disable heaters and set unused bit to 1 ?
        self.update_signals('BMSA_RPDO_CONTROL',
                            {'BMSA_CNTRL_UNUSED_BIT_1':1,
                            'BMSA_CNTRL_INHIBIT_HEATER': 1})
        
        sleep(0.1)
        # Disable control lock
        self.update_signals('BMSA_RPDO_CONTROL',
                            {'BMSA_CNTRL_LOCK':0})

        sleep(0.2)
        # Enable precharge and contactor close
        self.update_signals('BMSA_RPDO_CONT_PRECHG',
                            {'BMSA_CONTPRE_PRECHRG_CMD':2,
                            'BMSA_CONTPRE_CONTACTOR_CMD':4})

        sleep(0.1)
        # Enable precharge and contactor close p2????????
        self.update_signals('BMSA_RPDO_CONT_PRECHG',
                            {'BMSA_CONTPRE_PRECHRG_CMD':0x1202})

        # Do not proceed to control lock and precharge disable until contactor is shown as closed
        while not self.contactor_closed():
            sleep(0.1)

        # control lock
        self.update_signals('BMSA_RPDO_CONTROL',
                            {'BMSA_CNTRL_LOCK':1})
        
        # # disable precharge
        self.update_signals('BMSA_RPDO_CONT_PRECHG',
                            {'BMSA_CONTPRE_PRECHRG_CMD': 3})

    def open_contactor(self):
        self.update_signals('BMSA_RPDO_CONTROL',
                            {'BMSA_CNTRL_LOCK':0})
        sleep(0.1)
        bcb.update_signals('BMSA_RPDO_CONT_PRECHG',
                            {'BMSA_CONTPRE_CONTACTOR_CMD':0x05})
        bcb.update_signals('BMSA_RPDO_CONT_PRECHG',
                        {'BMSA_CONTPRE_PRECHRG_CMD':0})
        sleep(0.1)
        self.update_signals('BMSA_RPDO_CONTROL',
                            {'BMSA_CNTRL_LOCK':1})
        
if __name__ == '__main__':
    try:
        root = tk.Tk()
        root.geometry('200x200')

        bcb = bms_can()

        sn_text = tk.StringVar()
        sn_text.set("BCBM0000")
        btn_start = tk.Button(
            root,
            text = "Connect",
            command=bcb.init_network
        )
        btn_close_contactor = tk.Button(
            root,
            text = "Close Contactor",
            command=bcb.close_contactor
        )
        btn_open_contactor = tk.Button(
            root,
            text = "Open Contactor",
            command=bcb.open_contactor
        )
        bcb_sn_label = tk.Label(
            root,
            textvariable = sn_text,
        )

        btn_start.pack()
        btn_close_contactor.pack()
        btn_open_contactor.pack()
        bcb_sn_label.pack()

        root.mainloop()

    finally:
        try:
            bcb.update_signals('BMSA_RPDO_CONT_PRECHG',
                            {'BMSA_CONTPRE_CONTACTOR_CMD':0x05})
            bcb.update_signals('BMSA_RPDO_CONT_PRECHG',
                            {'BMSA_CONTPRE_PRECHG_CMD':0})
            sleep(0.1)
            bcb.network.sync.stop()
            bcb.network.disconnect()
        except Exception as e:
            print(Exception)