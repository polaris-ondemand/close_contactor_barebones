#!c:\hil\test\close_contactor_barebones\.venv\scripts\python.exe
# coding: utf-8

"""
See :mod:`can.logger`.
"""

from __future__ import absolute_import

from can.logger import main


if __name__ == "__main__":
    main()
