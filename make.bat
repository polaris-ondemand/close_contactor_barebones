@ECHO OFF
pyinstaller --onefile close_contactor_barebones.py
FOR /F "tokens=*" %%F IN ('"python get_ver.py"') DO SET var=%%F
set "message=close_contactor_barebones%var%"
copy "*.eds" ".\dist\*.eds" & copy "*.dbc" ".\dist\*.dbc"
cd dist

ren "close_contactor_barebones.exe" "%message%.exe"
if not exist "%message%" mkdir "%message%"
copy "*.eds" ".\%message%\*.eds"
copy "*.dbc" ".\%message%\*.dbc"
del "*.dbc" & del "*.eds"
move "%message%.exe" ".\%message%\%message%.exe"

@REM cmd /k